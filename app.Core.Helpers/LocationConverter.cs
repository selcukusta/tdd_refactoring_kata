﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace app.Core.Helpers
{
    public class LocationConverter : JsonConverter<Tuple<double, double>>
    {
        public override Tuple<double, double> ReadJson(JsonReader reader, Type objectType, Tuple<double, double> existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var values = ((string)reader.Value).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            return Tuple.Create(double.Parse(values.ElementAtOrDefault(0)), double.Parse( values.ElementAtOrDefault(1)));
        }

        public override void WriteJson(JsonWriter writer, Tuple<double, double> value, JsonSerializer serializer)
        {
            throw new NotImplementedException("Not implemented yet!");
        }
    }
}

﻿namespace app.Core.Helpers
{
    public static class WeatherForecastHelper
    {
        public static string TranslateDescriptionToTurkish(string description)
        {
            switch (description)
            {
                case "Snow": return "Karlı";

                case "Sleet": return "Karla Karışık Yağmur";

                case "Hail": return "Dolu Yağışı";

                case "Thunderstorm": return "Gök Gürültülü Fırtına";

                case "Heavy Rain": return "Sağanak Yağışlı";

                case "Light Rain":
                case "Showers": return "Hafif Yağışlı";

                case "Heavy Cloud": return "Yoğun Bulutlu";

                case "Light Cloud": return "Açık Bulutlu";

                case "Clear": return "Açık";
            }
            return string.Empty;
        }
    }
}

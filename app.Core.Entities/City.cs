﻿using app.Core.Helpers;
using Newtonsoft.Json;
using System;

namespace app.Core.Entities
{
    public class City
    {
        [JsonProperty("woeid")]
        public int Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("latt_long")]
        [JsonConverter(typeof(LocationConverter))]
        public Tuple<double, double> Location { get; set; }
    }
}

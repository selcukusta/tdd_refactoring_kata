﻿using System.Collections.Generic;

namespace app.Core.Entities
{
    public class WeatherForecast
    {
        public City Area { get; set; }
        public IList<Unit> Forecast { get; set; }
    }
}

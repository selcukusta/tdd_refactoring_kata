﻿using Newtonsoft.Json;
using System;

namespace app.Core.Entities
{
    public class Unit
    {
        [JsonProperty("weather_state_name")]
        public string Description { get; set; }
        [JsonProperty("weather_state_abbr")]
        public string IconCode { get; set; }
        [JsonProperty("applicable_date")]
        public DateTime Date { get; set; }
        [JsonProperty("min_temp")]
        public double MinimumTemperature { get; set; }
        [JsonProperty("max_temp")]
        public double MaximumTemperature { get; set; }
        [JsonProperty("the_temp")]
        public double CurrentTemperature { get; set; }
    }
}

﻿using app.Core.Entities;
using app.Infrastructure.Services;
using System.Web.Mvc;

namespace app.Presentation.Mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var forecast = WeatherForecastService.GetForecast();
            if(forecast != default(WeatherForecast))
            {
                return View(forecast);
            }
            return HttpNotFound();
        }
    }
}
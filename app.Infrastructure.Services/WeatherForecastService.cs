﻿using app.Core.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Caching;

namespace app.Infrastructure.Services
{
    public static class WeatherForecastService
    {
        internal const string BASE_URI = "https://www.metaweather.com/api/location/";
        internal const string CITY_PATH = "search/?query=istanbul";
        public static WeatherForecast GetForecast()
        {
            if (HttpContext.Current.Cache.Get("weather") != null)
            {
                return HttpContext.Current.Cache.Get("weather") as WeatherForecast;
            }

            try
            {
                using (WebClient cityClient = new WebClient())
                {
                    cityClient.BaseAddress = BASE_URI;
                    var cityJson = cityClient.DownloadString(CITY_PATH);
                    var response = JsonConvert.DeserializeObject<IList<City>>(cityJson);
                    if (response == null || !response.Any())
                    {
                        return default;
                    }

                    var city = response.FirstOrDefault();
                    if (city.Id <= 0)
                    {
                        return default;
                    }

                    using (WebClient forecastClient = new WebClient())
                    {
                        forecastClient.BaseAddress = BASE_URI;
                        var forecastJson = forecastClient.DownloadString(city.Id.ToString());
                        var weather = JObject.Parse(forecastJson).GetValue("consolidated_weather");
                        if(weather == null || !weather.HasValues)
                        {
                            return default;
                        }

                        var forecast = JsonConvert.DeserializeObject<IList<Unit>>(weather.ToString());
                        if (forecast == null || !forecast.Any())
                        {
                            return default;
                        }

                        var result = new WeatherForecast() { Area = city, Forecast = forecast };
                        return (WeatherForecast)HttpContext.Current.Cache.Add("weather", result, null, DateTime.Now.AddMinutes(15), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);             
                    }
                }
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }
    }
}

﻿using app.Infrastructure.Services;
using Xunit;

namespace app.Tests
{
    public class WeatherForecastTests
    {
        [Fact]
        public void FixIt()
        {
            var result = WeatherForecastService.GetForecast();
            Assert.NotEmpty(result.Forecast);
        }
    }
}
